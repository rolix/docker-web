FROM php:7.4-apache

# Update the package manager
RUN apt-get update

RUN apt-get install -y vim

# Install necessary packages
RUN apt-get install -y git libzip-dev unzip

# Install the PHP extensions
RUN docker-php-ext-install pdo_mysql zip

# Enable the Apache mod_rewrite
RUN a2enmod rewrite

# Enable the Apache mod_headers
RUN a2enmod headers

# Copy the Apache configuration file
COPY 000-default.conf /etc/apache2/sites-available/000-default.conf

# Set the working directory
WORKDIR /var/www/html

#RUN git clone https://x-token-auth:ATCTT3xFfGN0jBdDHvBtnENHZLl81lu90PZkQ4mPi6rRFo-rFpEgSPMVkLZpXvHDCt8043duhzpbHsy4ptoyEFr5L9swtkNfDx5JhwTl9L6K4S4LMS9fOA3yGD__dBEP-aayMFFer4Y0dIdvlGjJsL5mMLslyr-1zN8M5GVdJUdIXWrNbcbT3j8=4F4EC3A0@bitbucket.org/beterapp/beter-api.git
#RUN git clone https://beterapp-admin:f7HW4yDC7BRYmAxFyemQ@bitbucket.org/beterapp/beter-web.git

# Copy the application files
COPY ./src .

# Install composer
#RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Install the application dependencies
#RUN composer install

# Change the ownership of the files
RUN chown -R www-data:www-data /var/www/html
#

#RUN cp .env.example .env

# Expose the Apache port
EXPOSE 80

# Start the Apache service
CMD ["apache2-foreground"]
